from django.urls import path
from .views import show_list, show_details, create_todolist, update_todolist, delete_todolist


urlpatterns = [
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("<int:id>/edit/", update_todolist, name="todo_list_update"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/", show_details, name="todo_list_detail"),
    path("", show_list, name="todo_list_list"),
]
