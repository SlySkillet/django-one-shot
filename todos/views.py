from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm

# Create your views here.
def show_list(request):
    todo_list = TodoList.objects.all()

    context = {
        "todo_list": todo_list
    }

    return render(request, "todo_lists/list.html", context)


def show_details(request, id):
    detail = get_object_or_404(TodoList, id=id)

    context = {
        "detail": detail
    }

    return render(request, "todo_lists/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", id=detail.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }

    return render(request, "todo_lists/create.html", context)

def update_todolist(request, id):
    detail = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=detail)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", id=detail.id)
    else:
        form = TodoListForm(instance=detail)

    context = {
        "form": form
    }

    return render(request, "todo_lists/update.html", context)


def delete_todolist(request, id):
    detail = TodoList.objects.get(id=id)
    if request.method == "POST":
        detail.delete()
        return redirect("todo_list_list")

    return render(request, "todo_lists/delete.html")
